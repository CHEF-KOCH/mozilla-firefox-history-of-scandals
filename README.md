[[_TOC_]]


### [Motivation](#motivation)

While every small incident gets criticized in Chrome, Brave and Edge, Mozilla simply seems to get away with pretty much everything they do.  Given the fact that Mozilla wants to have [high ethical standards](https://foundation.mozilla.org/en/initiatives/great-tech-great-responsibility/addressing-ethical-issues-tech-worker/) and claims that they are a [people-first Browser](https://web.archive.org/web/20220215151645/https://pbs.twimg.com/media/FLpI2m-XIAEfWX1?format=png&name=large) it is only fair to inspect them closely.

We need to differ here because there’s the legally-non-profit Mozilla Foundation and the legally-for-profit Mozilla Corporation, I mention it to avoid running into a mixture of interests, what is actually meant is usually explained within the provided links.

Links that Mozilla advocates often like to down-talk, hide or to if possible distract from it by smearing the ones who post actual evidence that there might be something wrong with Mozilla, this is nothing new and this all happened multiple times to multiple people who just tried to show what is or could be controversial.

Even the [Wikipedia](https://en.wikipedia.org/wiki/Firefox) article regarding Mozilla and Firefox is far from objective nor [accurate](https://archive.fo/W1K0z). That practical applies to almost every Wikipedia Browser article because people are highly based in defending their beloved Browser and they often end up arguing things down until it is removed from such pages, the Wikipedia edit history and discussion talk page proves me right.

But whatever, I like to keep an eye on Mozilla and provide an objective but fair overview of all past scandals, this basically mean I will update this article whenever there is something new to mention.

This little blog post here is not meant to bash Mozilla or to say everything they do is wrong, it is more like a small review of past incidents and questionable decisions coming directly from Mozilla or people involved into the development, marketing, scripting etc.


I like to add that we all do mistakes and Mozilla is absolute no exception in this regard, but I simply dislike the hypocrisy that gets wrongfully communicated to the outside world.


### [Definition of a scandal](#definition-of-a-scandal)

Like most terms, this can be bent, with scandals we classify things under privacy invasive, against user will or impacts that affected the average but daily Firefox users. This does not include power users as well as users who have 20 Browsers installed and barely use Firefox as their main Browser, because they might not be impacted because Mozilla released quickly an update to address listed things.

I try to split this in specific categories. To make it easier to comprehend.


### [Resolved issues](#resolved-issues)

Same like all or most scandals, all the mentioned stuff below might be already fixed and one of the main reason why you should always use the latest software versions.

There are more reasons and controversies but this is not substantial in this little blog post and therefore irrelevant, which means they do not even get listed in the first place.


### [Info and Feedback](#info-and-feedback)

The Firefox community can get very toxic defending their beloved Browser and the Mozilla Corporation. As always, this is a oversimplification and not everyone is like that. Keep that in mind.

Before you curse me, my soul or link this against my will into forum xyz please contact me first, so that I have a chance to address feedback BEFORE you put this on any other platform.

-----------------------------------


## [Advertising](#advertising)
- [Mozilla will put ads directly into Firefox](https://blog.mozilla.org/advancingcontent/2014/02/11/publisher-transformation-with-users-at-the-center).
- [“Privacy Preserving Attribution for Advertising“ with Facebook's Meta IPA](https://blog.mozilla.org/en/mozilla/privacy-preserving-attribution-for-advertising/), this article might be more accurate [Mozilla and Meta (FB) are working together on “privacy” ads](https://www.xda-developers.com/mozilla-meta-interoperable-private-attribution/).
- [Disney Movie Ad on Firefox Upgrade Front Page](https://twitter.com/CKsTechNews/status/1501627658072952837).



## [Bypassing Proxy System Settings](#bypassing-proxy-system-settings)
- [Firefox 91.1 bypasses the system proxy settings](https://ghostarchive.org/news/pqfFe/web/ghost/).



## [Certificates](#certificates)
- [Certificate-based Mozilla Persona IdP](http://unmitigatedrisk.com/?p=328), killed years [laters](https://wiki.mozilla.org/Identity/Persona_Shutdown_Guidelines_for_Reliers#FAQs).
- [Firefox browser trusts DarkMatter CA certificate to cause security industry controversy](https://bugzilla.mozilla.org/show_bug.cgi?id=1427262) (see [here](https://www.reuters.com/investigates/special-report/usa-spying-raven/) how it could be abused) _However, this one is questionable and controversial because there is no audit or sign that this was abused.
- [Mozilla contains unidentified root certificates](https://bugzilla.mozilla.org/show_bug.cgi?id=549701)
- [Mozilla re-trusting an SSL vendor previously revoked for mass cert fraud](https://bugzilla.mozilla.org/show_bug.cgi?id=1311832#c16)
- [Mozilla shipping SSL root certificate, has no idea how it got there](http://groups.google.com/group/mozilla.dev.security.policy/browse_thread/thread/b6493a285ba79998/26fca75f9aeff1dc)



## [DRM](#drm)
- [DRM adoption](https://www.theguardian.com/technology/2014/may/14/firefox-closed-source-drm-video-browser-cory-doctorow) see [FSF condemns partnership between Mozilla and Adobe to support DRM](https://www.fsf.org/news/fsf-condemns-partnership-between-mozilla-and-adobe-to-support-digital-restrictions-management). [Background](https://www.theguardian.com/technology/2014/may/14/firefox-closed-source-drm-video-browser-cory-doctorow).



## [Extension based Scandals](#extension-based-scandals)
- [Firefox Cliqz scandal](https://old.reddit.com/r/privacy/comments/7545i8/psa_in_the_wake_of_the_firefox_cliqz_scandal_you/)
- [Firefox Shield Studies](https://wiki.mozilla.org/Firefox/Shield/Shield_Studies) - These studies are separate from the normal telemetry.
- [Firefox is spying on your use of certain websites.](https://old.reddit.com/r/firefox/comments/anxfz8/firefox_is_spyware_extension_recommendation/)
- [Mozilla hit with takedown request for anti-paywall addons](https://github.com/nextgens/anti-paywall/issues/109#issuecomment-441097828), while they claim to have better standards than Google, they do exactly the same, when their ass is on the line they remove add-ons. Standing up for the little ones and anti-censorship, not so much.
- [Mr. Robot/Studies scandal](https://old.reddit.com/r/firefox/comments/7sisux/reminder_its_been_six_weeks_since_the_mr/)
- [Questionable add-on policies](https://web.archive.org/web/20181203133035/https://developer.mozilla.org/en-US/docs/Mozilla/Add-ons/AMO/Policy/Reviews).
- [Unknown Mozilla dev addon "Looking Glass 1.0.3" on browser](https://support.mozilla.org/en-US/questions/1194583)
- [Mozilla loses in Chinese court, forced to ban adblockers opens the way to justify on legal precedence case getting rid of adblockers (2022)](https://twitter.com/lomomcat/status/1505836402256146435)



## [Firefox Focus](#firefox-focus)
- [Firefox Focus Privacy Scandal](https://www.ghacks.net/2017/02/12/firefox-focus-privacy-scandal/).



## [Killed by Mozilla](#killed-by-mozilla)
- [A list of discontinued Mozilla products and services](https://killedbymozilla.com/).



## [Logo](#logo)
- [Firefox logo controversy finally addressed after years by Mozilla](https://www.creativebloq.com/news/mozilla-addresses-firefox-logo-controversy).



## [Mozilla and Censorship](#mozilla-and-censorship)
- [Mozilla Explains: Why Does YouTube Recommend Conspiracy Theory Videos](https://foundation.mozilla.org/en/blog/mozilla-explains-why-does-youtube-recommend-conspiracy-theory-videos/). The problem on such article is that YouTube has a very detailed guidance on how they deal with disinformation and misinformation.
- [Mozilla defends Trump twitter ban](https://blog.mozilla.org/en/mozilla/we-need-more-than-deplatforming/).



## [Mozilla wants to intervene into Politics](#mozilla-wants-to-intervene-into-politics)
- [Contributing to the European Commission’s review of digital competition](https://via.hypothes.is/https://blog.mozilla.org/netpolicy/2018/09/28/contributing-to-the-european-commissions-review-of-digital-competition/).
- [EU Code published: another step forward in the fight against disinformation](https://via.hypothes.is/https://blog.mozilla.org/netpolicy/2018/09/26/eucodeondisinfo/).
- [Explore how to help keep the web open](https://archive.li/uWk34) which supports directly [freeganism](https://en.wikipedia.org/wiki/Freeganism).
- [Getting serious about political ad transparency with Ad Analysis for Facebook](https://via.hypothes.is/https://blog.mozilla.org/netpolicy/2018/10/18/getting-serious-about-political-ad-transparency-with-ad-analysis-for-facebook/).
- [Mozilla reacts to EU Parliament vote on copyright reform](https://archive.fo/HUSfN).
- [Mozilla sending political ads via Firefox push notification](https://www.reddit.com/r/firefox/comments/hx8737/why_did_i_just_receive_an_ad_for_mozillas_blog/).
- [The future of online advertising – Mozilla panel discussion at ICDPPC](https://blog.mozilla.org/netpolicy/2018/09/19/the-future-of-online-advertising-mozilla-panel-discussion-at-icdppc/).
- [We need more than deplatforming](https://blog.mozilla.org/en/mozilla/we-need-more-than-deplatforming/) - If you do not understand what this is about read [this](https://www.linuxadictos.com/en/mozilla-se-sigue-equivocando-necesitamos-un-mejor-navegador-no-correccion-politica.html) instead, see [here](https://archive.fo/yeb5c).



## [Misleading Bugzilla entries](#misleading-bugzilla-entries)
Cheating on bug tickets and issues tickets to hide and manipulate things is not invented by Mozilla nor is it a Mozilla only problem, practical everyones does it. GitHub hides search entries, Google closes tickets and re-create them under different name or hide them from the search index. However entirely mislabeling tickets is really weird way to twist reality.

- [Firefox v98.0.1 removed Yandex search option and used misleading bug name to hide it](https://vc.ru/services/379414-poiskovik-yandeksa-propal-iz-nastroek-brauzera-mozilla-firefox). The issue [ticket](https://bugzilla.mozilla.org/show_bug.cgi?id=1759009) does not imply that at any point nor did an moderator changed anything to update the wrong information.



## [Other scandals](#other-scandals)
- [Firefox blocks Ad-filtering add-ons (uBlock Origin, AdBlock) in China since 2022](https://news.ycombinator.com/item?id=30740366).
- [Firefox takes screenshots of your HTTPS data - Mozilla says thats ok](https://bugzilla.mozilla.org/show_bug.cgi?id=755996).
- [Former Mozilla CTO detained at US border and denied a lawyer](https://www.zdnet.com/article/former-mozilla-cto-detained-at-us-border-and-denied-a-lawyer/).
- [Google and Mozilla are failing to support browser extension developers](https://armin.dev/blog/2019/08/supporting-browser-extension-developers/)
- [Google-Firefox search deal gives Mozilla more money to push privacy](https://www.cnet.com/tech/services-and-software/google-firefox-search-deal-gives-mozilla-more-money-to-push-privacy/), maybe better explained here [Mozilla Signs Lucrative 3-Year Google Search Deal for Firefox](https://www.pcmag.com/news/mozilla-signs-lucrative-3-year-google-search-deal-for-firefox)
- [Links and mentioning to an open source recruitment project that is dead since March 2015](https://archive.fo/lJU3L).
- [Mozilla - Devil Incarnate](https://digdeeper.neocities.org/ghost/mozilla.html) - [HackerNews Censors article that criticizes Mozilla](https://news.ycombinator.com/item?id=30233110), [resubmission](https://news.ycombinator.com/item?id=30233369).
- [Mozilla admits to mishandling Comodo disclosure](http://blog.mozilla.com/security/2011/03/25/comodo-certificate-issue-follow-up/).
- [Mozilla and the ‘Planet-Incinerating Ponzi Grifters’](https://thenewstack.io/mozilla-and-the-planet-incinerating-ponzi-grifters/)
- [Mozilla expands advertising experiment to many more Firefox users](http://www.cnet.com/news/mozilla-expands-advertising-experiment-to-many-more-firefox-users/)
- [Mozilla illegally leaked my startup data to my competitor](https://medium.com/@dwebox/mozilla-illegally-leaked-my-company-data-to-our-competitor-9f8438bfdcab).
- [Mozilla now banning people for grumpy bug reports](https://bugzilla.mozilla.org/show_bug.cgi?id=668655#c1).
- [Mozilla re-enables TLS 1.0 and 1.1 because of Coronavirus (and Google)](https://www.ghacks.net/2020/03/21/mozilla-re-enables-tls-1-0-and-1-1-because-of-coronavirus-and-google/).
- [Mozilla responds to Booking.com Snippet Concerns; “It was not a paid placement or advertisement. We are continually looking for more ways to say thanks for using Firefox."](https://old.reddit.com/r/privacytoolsIO/comments/abfgj5/mozilla_responds_to_bookingcom_snippet_concerns/)
- [Opera claims ex-employee took trade secrets to Mozilla, sues him for $3.4m](https://thenextweb.com/insider/2013/04/29/opera-claims-former-employee-gave-away-trade-secrets-to-mozilla-sues-him-for-3-4m).
- [Termination of Mozilla CEO Likely Violated California Law](https://www.vtzlawblog.com/2014/04/articles/discrimination/termination-of-mozilla-ceo-likely-violated-california-law/).
- [Weird Wiki statements](https://archive.fo/ZJWpV).



## [Password Security](#password-security)
- [Mozilla accidentally posts usernames and password hashes](https://www.tgdaily.com/security-features/53267-mozilla-accidentally-posts-usernames-and-password-hashes)
- [Mozilla uses server-hosted JavaScript to 'secure' user passwords](https://accounts.firefox.com/scripts/0f968594.main.js)



## [Pocket](#pocket)
- [Mozilla Admits to Revenue Sharing Arrangement with Pocket](http://www.wired.com/2015/12/mozilla-is-flailing-when-the-web-needs-it-the-most/).
- [Mozilla responds to Firefox user backlash over Pocket integration](http://venturebeat.com/2015/06/09/mozilla-responds-to-firefox-user-backlash-over-pocket-integration/), see [background](https://old.reddit.com/r/firefox/comments/3vijxi/psa_mozilla_does_make_money_off_of_the_pocket/cxo8v14/?context=3), after years of [fighting to remove or build a version without pocket integrated](https://venturebeat.com/2015/11/12/mozilla-has-no-plans-to-offer-firefox-without-pocket/), they [integrated it into accounts](https://support.mozilla.org/en-US/kb/pocket-firefox-account-migration).
- [Mozilla tries ads in Firefox again, now powered by Pocket recommendations](https://www.cnet.com/news/mozilla-tries-ads-in-firefox-again-now-powered-by-pocket-website-recommendations/)
- [Pocket's privacy policy is a nightmare](https://ghostarchive.org/archive/69mcK?kreymer=false), please see [here](https://archive.fo/36aIm).



## [PWA](#pwa)
- [Firefox 85 Is Here, but Mozilla Is Killing PWA Features](https://www.thurrott.com/cloud/web-browsers/mozilla-firefox/246716/firefox-85-is-here-but-mozilla-is-killing-pwa-features)



## [Questionable CEO Decisions](#questionable-ceo-decisions)
- [Firefox OS Successor: Mozilla and KaiOS Announce Partnership](https://www.kaiostech.com/press/kaios-technologies-and-mozilla-partner-to-enable-a-healthy-mobile-internet-for-everyone/).
- [Mozilla CEO resignation raises free-speech issues](https://eu.usatoday.com/story/news/nation/2014/04/04/mozilla-ceo-resignation-free-speech/7328759/).
- [Mozilla CEO: Premium version of Firefox coming this fall](https://www.zdnet.com/article/mozilla-ceo-premium-version-of-firefox-coming-this-fall/), see [Mozilla Plans to Sell Ads in Firefox Browser](http://daringfireball.net/linked/2014/02/16/mozilla) or [here](https://archive.fo/qpVPY).
- [Mozilla CEO: Premium version of Firefox coming this fall](https://www.zdnet.com/article/mozilla-ceo-premium-version-of-firefox-coming-this-fall/).
- [Mozilla Coil - A $100 Million Investment to Reshape the Economics of the Web](https://foundation.mozilla.org/en/blog/100-million-investment-reshape-economics-web/).
- [Mozilla Executive: We're Not Interested In Enterprise Level Users](http://www.pcmag.com/article2/0,2817,2387514,00.asp?f=2).
- [Mozilla Fires 250 Employees, 25 Percent of Existing Workforce](https://www.extremetech.com/computing/313658-mozilla-fires-250-employees-25-percent-of-existing-workforce), check this out for a [nutshell](https://calpaterson.com/mozilla.html)
- [Mozilla Persona (BrowserID) is a step in the wrong direction](https://www.opine.me/mozilla-persona-browserid-is-a-step-in-the-wrong-direction/).
- [Mozilla Rally to “fight big tech”](https://rally.mozilla.org/) - I could basically list all studies here because they are all worthless and solve absolute nothing. The web is in a rough state but doing research on user data isn't going to solve anything. Why the CEO wants to research this and waste money and resources for this is beyond any logic.
- [Mozilla Will Stop Developing and Selling Firefox OS Smartphones](http://techcrunch.com/2015/12/08/mozilla-will-stop-developing-and-selling-firefox-os-smartphones/).
- [Mozilla Will Stop Developing and Selling Firefox OS Smartphones](https://techcrunch.com/2015/12/08/mozilla-will-stop-developing-and-selling-firefox-os-smartphones/).
- [Mozilla and Epic Announce Unreal Engine for the Web](https://blog.mozilla.org/blog/2013/03/27/mozilla-is-unlocking-the-power-of-the-web-as-a-platform-for-gaming/).
- [Mozilla could walk away from Yahoo deal and get more than $1B](https://www.recode.net/2016/7/7/12116296/marissa-mayer-deal-mozilla-yahoo-payment).
- [Mozilla rejects US government request to remove add-on](http://www.h-online.com/open/news/item/Mozilla-rejects-US-government-request-to-remove-add-on-1238743.html).
- [Mozilla shuts down servo team](https://mobile.twitter.com/asajeffrey/status/1293220656339988483).
- [Mozilla starts accepting Bitcoin donations since 2014](https://blog.mozilla.org/en/mozilla/mozilla-now-accepts-bitcoin/).
- [Mozilla to remove Firefox version numbers](http://www.extremetech.com/internet/92792-mozilla-takes-firefox-version-number-removal-a-step-further), this promise never happened, they followed the Chrome versions scheme.
- [Mozilla’s Rejection of NativeClient Hurts the Open Web](http://chadaustin.me/2011/01/mozillas-rejection-of-nativeclient-hurts-the-open-web/).
- [Moznet IRC is dead; long live Mozilla Matrix](https://matrix.org/blog/2020/03/03/moznet-irc-is-dead-long-live-mozilla-matrix).
- [XUL deprecation](https://web.archive.org/web/20181128124935/https://developer.mozilla.org/en-US/docs/Mozilla/Add-ons/WebExtensions/Comparison_with_XUL_XPCOM_extensions), lots of respectable [extensions developer were unhappy](https://archive.fo/0nuvK) with this [decision](https://archive.fo/DR88t).



## [Questionable Partnership](#questionable-partnership)
- [Cloudflare partnership](https://web.archive.org/web/20200201115329/https://hacks.mozilla.org/2018/05/a-cartoon-intro-to-dns-over-https/) with [questionable outcomings](https://archive.is/UvEG6). Cloudflare is controversial because of the nature on how their [MITM proxy works](https://web.archive.org/web/20200108144248/https://git.redxen.eu/dCF/deCloudflare/src/branch/master/readme/en.md). The [privacy agreement they have is controversial](https://web.archive.org/web/20200129092336/https://developers.cloudflare.com/1.1.1.1/commitment-to-privacy/privacy-policy/firefox/) because [Cloudflare still can see a lot](https://archive.is/UvEG6). Most say to [disable DoH](https://web.archive.org/web/20191205204339/https://undeadly.org/cgi?action=article;sid=20190911113856) because there are downsides.
- [Firefox And Pixar Animation Studios team up to help you show your true colors](https://blog.mozilla.org/en/products/firefox/true-colors-with-firefox-pixar-animation-studios-turning-red/)
- [Firefox Data Removal Pilot another Partner called Kanary who wants your Data](https://monitor.firefox.com/remove-faq)
- [Firefox OS Successor: Mozilla and KaiOS Announce Partnership](https://www.kaiostech.com/press/kaios-technologies-and-mozilla-partner-to-enable-a-healthy-mobile-internet-for-everyone/)
- [Mozilla Discloses Past Relationships with Facebook](https://blog.mozilla.org/blog/2018/06/11/our-past-work-with-facebook/)
- [Mozilla Partners with Telefonica for Boot 2 Gecko Phone](http://blog.mozilla.com/blog/2012/02/27/mozilla-in-mobile-the-web-is-the-platform/).
- [Mozilla partners with Nvidia to democratize and diversify voice technology](https://blog.mozilla.org/blog/2021/04/12/mozilla-partners-with-nvidia-to-democratize-and-diversify-voice-technology/)
- [Mullvad because of money](https://ghostarchive.org/news/PSS3i/web/ghost/). There are also [some concerns](https://ghostarchive.org/news/OWqwD/web/ghost/).
- [ProtonVPN partnership](https://archive.fo/MflM2), because of [money](https://archive.fo/MnvMD).
- [Firefox depends to 90 Percent on Google](https://www.googlewatchblog.de/2021/12/mozilla-google-abhaengigkeit-mio-dollar/).



## [Search Engine](#search-engine)
- [Anti-privacy friendly search engines by default](https://support.mozilla.org/en-US/questions/1284545). LibreWolf, a fork addresses this by delivering a pre-configured Browser with a better search engine set by default.
- [Mozilla tests Microsoft Bing as the default Firefox search engine](https://www.bleepingcomputer.com/news/software/mozilla-tests-microsoft-bing-as-the-default-firefox-search-engine/), [background](http://arstechnica.com/microsoft/news/2009/12/mozilla-exec-urges-firefox-users-ditch-google-for-bing).
- [Shady marketing](https://archive.fo/gRATt).



## [The Phoenix Project](#the-phoenix-project)
- [Mozilla Phoenix project got abandoned, people protested](https://web.archive.org/web/20021004012820/http://www.mozilla.org/projects/phoenix/) and Mozilla did not hold their promise.



## [Telemetry, Trackers or strange connections](#telemetry--trackers-or-strange-connections)
- [Bug in Firefox data collection causes 100 Percent CPU usage](https://ghostarchive.org/news/UADBF/web/ghost/)
- [Firefox Now Sends Your Address Bar Keystrokes to Mozilla](https://www.howtogeek.com/760425/firefox-now-sends-your-address-bar-keystrokes-to-mozilla/)
- [Firefox tracks users with Google Analytics in the add-on settings](https://github.com/mozilla/addons-frontend/issues/2785)
- [Google Analytics is used to track users](https://ghostarchive.org/archive/4X56U?kreymer=false)
- [How can I stop Firefox from constantly connecting to self-repair.mozillia.org](https://ghostarchive.org/archive/bHz3f?kreymer=false)
- [Metrics Mozilla collects](https://ghostarchive.org/archive/ty2Xt?kreymer=false)
- [Mozilla Installs Scheduled Telemetry Task on Windows with Firefox 75](https://www.ghacks.net/2020/04/09/mozilla-installs-scheduled-telemetry-task-on-windows-with-firefox-75/)
- [Mozilla collects data](https://archive.fo/lKR5b), see [here](https://archive.fo/cPLIE) for more [detailed collection](https://archive.fo/2nvJZ).
- [Mozilla didn't whitelist a massive list of domains](https://ghostarchive.org/archive/PLqhJ?kreymer=false)
- [Mozilla's page talking about privacy has Google analytics enabled](https://shapeoftheweb.mozilla.org/trust/governmentSurveillance)
- [Snippets Service Data Collection](https://ghostarchive.org/archive/1Ad5D?kreymer=false)
- [Unintentional collection of user-edited search engine values](https://bugzilla.mozilla.org/show_bug.cgi?id=1752317)
- [Each Firefox download has a unique identifier called dltoken (2022)](https://bugzilla.mozilla.org/show_bug.cgi?id=1677497#c0), this is similar to the [Chromes RLZ](https://www.chromium.org/developers/design-documents/extensions/proposed-changes/apis-under-development/rlz-api/) feature
- [Mozilla tracks users by (WiFi) SSID name, unless you add _nomap (2022)](https://location.services.mozilla.com/optout)

