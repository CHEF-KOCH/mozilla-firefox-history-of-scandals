## [Feedback on Mozilla Ethics](#feedback-on-mozilla-ethics)
- [Dear Mozilla, Please Don't Kill HTML5 Video](http://briancrescimanno.com/2010/03/17/dear-mozilla-please-dont-kill-html5-video/).
- [Firefox Cannot Be Trusted at the Hands of Today’s Mozilla Management](http://techrights.org/2021/08/05/lousy-mozilla-management/).
- [Free software ain't free to make, pal, it's expensive: Mozilla to bankro](https://www.theregister.co.uk/2019/06/11/mozilla_firefox_premium_services/).
- [Mozilla Contributor Calls Mozilla CEO Open Letter Hypocrisy](https://benjaminkerensa.com/2015/07/30/unnecessary-finger-pointing).
- [Mozilla Is Becoming Evil](https://unixsheikh.com/articles/mozilla-is-becoming-evil-be-careful-with-firefox.html).
- [Mozilla’s Add-on Policy Is Hostile to Open Source](https://tomdale.net/2014/11/mozillas-add-on-policy-is-hostile-to-open-source/).
- [OkCupid Takes Public Stand Against New Mozilla CEO](https://recode.net/2014/03/31/okcupid-takes-public-stand-against-new-mozilla-ceo/).
- [On our Abusive Relationship with Mozilla’s Firefox](https://ruzkuku.com/texts/moz-rel.html).
- [The Tragedy of Mozilla](https://medium.com/p/7645a4bf8a2).
- [Why I'm Boycotting Mozilla Products](https://jeffknupp.com/blog/2014/03/29/why-im-boycotting-mozilla-products/).


## [Toxic Bias and Fanboyism](#toxic-bias-and-fanboyism)
- [Arkenfox community](https://web.archive.org/web/20211103093350/https://chefkochblog.wordpress.com/2021/07/15/destroying-the-fact-checkers-with-facts/) that are bunch of trolls who harass, dox and try to discredit others who do a better job. Wrongfully claiming I and possible others would steal something when they steal from Bugzilla and tor issue tracker project all day long and then even trying to convert Firefox into Tor Browser, claiming it is more secure and privacy while it simply often only disables stuff which results in website breakages and other problems which overall is false sense of security.
- [Fans trying to frame me, posting only details that benefits them](https://web.archive.org/web/20220211093530/https://lemmy.ml/post/172121).
- [Filipus Klutiero scandal](https://archive.fo/WGi8K), see [here](https://archive.fo/U2bZl), [claiming the ban was justified](https://archive.fo/K2MKo) which was not.
- [See JavaScript toggle scandal](https://web.archive.org/web/20181012153654/https://bugzilla.mozilla.org/show_bug.cgi?id=873709).
- [Some fans trying to spread misinformation to make Firefox look better](https://chef-koch.bearblog.dev/brave-the-false-sensation-of-privacy-nonsense-article-debunked-with-actual-facts), even after debunking them they let it stand or pretend they would update their nonsense to address it. There are lots of false articles out there designed to convince people to switch to Mozilla with based opinions.
- [The toxicity is one of the reasons why people leave Mozilla](https://archive.fo/VDD73). Before someone question it, there are many topics on this, [even within their own subreddit](https://old.reddit.com/r/firefox/comments/ns7z6s/this_has_become_an_awful_community_completely/), also see here for [more](https://old.reddit.com/r/firefox/comments/bkz2qx/i_love_firefox_but_im_starting_to_dislike_the/).
- [Weird complaints about coding languages](https://archive.fo/BvOlf).
