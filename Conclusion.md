## [My conclusion 2022](#my-conclusion-2022)

Cherry picking arguments and then claim Browser xyz is less secure, less private or entirely flawed will not work because you will find on every single Browser stuff to criticize or a history of invasive scandals. Mozilla is clearly no exception here, as proven.


Browsers should not be seen as a perfect role-model for the most perfect software, instead we should focus on how fast things are getting fixed because the next scandals and flaws will occur because nothing is perfect and never will be. People make mistakes, people sometimes have a bad day or they have different motivations.


In my experience extremist people often desperately trying to defend their beloved Browser because they want other people to convince to join them and their ideas, without any regard if that makes sense or not because the best Browser or software for that matter is pretty much pointless if you - for whatever reasons - do not like it or do not feel comfortable with it then putting pressure on the average user will do no good and he certainly will switch back to old habits. Some users use Vivaldi because it offers some UI improvements over vanilla Chromium, others entirely only care about the privacy aspects. Personally I do not think that makes an killer argument because Chrome, Brave, Firefox and all other bigger Browsers constantly evolve and adopt to new threats. I believe a good Browser is defined by its community and the developers interacting with it.


What is left, the direction.


The direction maybe plays the biggest role - to some people. Some do not want to support Google, some dislike scandals and try to use the Browser with only a few scandals, others maybe do not like the CEO, or have other reasons to prefer x over y.


Web itself is not perfect and this automatically means it is hard to find a specific direction, claiming to rescue, or safe the web or make it better is AFAIK a charitable idea but in the real-world far from possible. You need to find a mid-way and then think of people who just doing their job or people who just like what they do.


Focusing on scandals does not help anybody at all and we should focus on upcoming issues, fix them together and then point out how fast things got fixed to display that the community as well as the developers behind it care about such things.


I wanted to provide a clean overview about scandals we had and I think I did that very well, some things got fixed - some more faster than others.

Other things never got fixed and instead they simply got replaced.

Mozilla same like Google and everyone else does make mistakes and cherry picking every mistake will lead to no positive improvement for all of us. We, as community need to objectively and unbiased review historic events and try to make it better, as community, and set positive examples to others so that possible controversies are avoided in the first place, or so that people that are not entirely involved into tech can easily comprehend the good parts from the bad parts.

History is therefore important to reveal true motivations as well as learn from past - mistakes - to make it better and set higher standards for all of us.

------------